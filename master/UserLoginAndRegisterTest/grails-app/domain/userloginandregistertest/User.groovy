package userloginandregistertest

class User {
    String login
    String name
    String password

    static constraints = {
        login(size: 1..30)
        name(blank: true)
        password(size: 3..30)
    }
}
