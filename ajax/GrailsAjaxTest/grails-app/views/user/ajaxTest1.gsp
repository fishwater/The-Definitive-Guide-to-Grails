<%--
  Created by IntelliJ IDEA.
  User: Ajian
  Date: 13-12-27
  Time: 下午5:27
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title></title>
  <g:javascript library="jquery" />
  <r:layoutResources />
</head>
<body>
    <script type="text/javascript">
        function inputData(data){
            var h = "<ul>";
            $.each(eval(data),function(i,d){
                h+="<li>"+ d.id+" "+ d.name+"</li>";
            });
            h+="</ul>";
            $("#allDateDIV").html(h);
        }
        function showAll(){
            $.ajax({
                type:'get',
                url:'<g:createLink controller="user" action="showByAjax" />',
                success:function(data){
                    inputData(data);
                },error:function(e){
                    alert(e);
                }
            })
        }
    </script>
    <input type="button" value="show All" onclick="showAll()" />
    <br/><br/>
    <g:remoteLink method="get" controller="user" action="showByAjax" onSuccess="inputData(data)" >show All</g:remoteLink>
    <br/><br/>
    <g:link controller="user" action="list">to list page</g:link>
    <div id="allDateDIV"></div>

</body>
</html>