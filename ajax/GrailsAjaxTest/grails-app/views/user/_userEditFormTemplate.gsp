<g:formRemote name="updateForm" url="[controller:'user',action:'changeOne']" onSuccess="successData(data,textStatus)" onerror="errorData(errorThrown)" >
    <h2>Edit User</h2>
    <g:hiddenField name="id" value="${user.id}" />
    name:<g:textField name="name" value="${user.name}" />
    <g:submitButton value="OK" name="submit" />
</g:formRemote>
<script type="text/javascript">
    function successData(data,textStatus){
        if(textStatus=='success'){
            alert(data);
            location.href='<g:createLink controller="user" action="list"/> ';
        }
    }
    function errorData(data){
        alert(data);
    }
</script>