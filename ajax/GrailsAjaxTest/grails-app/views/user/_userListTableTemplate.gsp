<script type="text/javascript">
    function showOneData(data){
        $("#editDIV").html(data);
    }
    function deleteOK(data){
        alert(data);
        location.href='<g:createLink controller="user" action="list"/> ';
    }
</script>
<table border="1">
    <tr>
        <th>number</th>
        <th>name</th>
        <th>action</th>
    </tr>
    <g:each in="${list}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>
                <g:remoteLink method="get" controller="user" action="getOne" id="${user.id}" onSuccess="showOneData(data)">edit</g:remoteLink>
                &nbsp;&nbsp;&nbsp;
                <g:remoteLink controller="user" action="delOne" id="${user.id}" onSuccess="deleteOK(data)">delete</g:remoteLink>
            </td>
        </tr>
    </g:each>
</table>
